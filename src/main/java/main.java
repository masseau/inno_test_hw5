import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine().replaceAll("\\s+", "");

        Pattern pattern = Pattern.compile("^(\\d+)(\\D)(\\d++)$");
        Matcher matcher = pattern.matcher(line);

        boolean exp = matcher.find();
        if (exp == true) {
            double n1 = Double.parseDouble(matcher.group(1));
            double n2 = Double.parseDouble(matcher.group(3));
            String sign = matcher.group(2);


            if (Objects.equals(sign, "*")) {
                System.out.println(n1*n2);
            }
            else if (Objects.equals(sign, "/") && (n2 != 0)){
                System.out.println(n1/n2);
            }
            else if (Objects.equals(sign, "+")){
                System.out.println(n1+n2);
            }
            else if (Objects.equals(sign, "-")){
                System.out.println(n1-n2);
            }
            else if (Objects.equals(sign, "/") && (n2 == 0)){
                System.out.println("Деление на ноль. Так нельзя-__-");
            }
            else if (!Objects.equals(sign, "+") || !Objects.equals(sign, "-")|| !Objects.equals(sign, "*")|| !Objects.equals(sign, "/")){
                System.out.println("Введен символ отличный от +-*/");
            }
        }
        else {
            System.out.println("Выражение не соответсвует условию число-знак-число");
        }

    }
}
